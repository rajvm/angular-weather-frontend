import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherapiService {

  constructor(private http: HttpClient) {}

  getWeather(location){
  //  return this.http.get(
  //     'http://localhost:3000/city/getWeather/'+ location
  // );
  return this.http.get(
    'https://65ene4ust8.execute-api.us-east-1.amazonaws.com/city/getWeather/'+ location
  );
  }

  getAutoCompleteCity(location){

  //    return this.http.get(
  //      'http://localhost:3000/city/getAutoCompleteCityName?city='+ location
  //  );
   return this.http.get(
    'https://65ene4ust8.execute-api.us-east-1.amazonaws.com/city/getAutoCompleteCityName?city='+ location
);
   }

   getSubscribeWeatherAlert(location){
  //   return this.http.get(
  //      'http://localhost:3000/city/updatePreferredForCity?city='+ location + '&isPreferred=1'
  //  );
   return this.http.get(
    'https://65ene4ust8.execute-api.us-east-1.amazonaws.com/city/updatePreferredForCity?city='+ location + '&isPreferred=1'
);
   }

}
