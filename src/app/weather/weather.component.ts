import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { WeatherapiService } from "../weatherapi.service";

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
  public weatherSearchForm: FormGroup;
  public weatherData: any;
  public subscribeAlert: any;
  isLoadingResult: boolean;

  constructor(private formBuilder: FormBuilder,private weatherapiService: WeatherapiService) { }

  ngOnInit(): void {
    this.weatherSearchForm = this.formBuilder.group({
      location: ['']
    });
  }
  sendToWeatherApi(formValues) {
    let locName ;
    if(!formValues.location.name){
      locName = formValues.location;
    }else{
      locName = formValues.location.name;
    }
    console.log(locName);
    this.weatherapiService
      .getWeather(locName)
      .subscribe(data =>{ this.weatherData = data;
      console.log(this.weatherData);
      });
  }
  subscribeToWeatherAlert(){
    const location = document.getElementById('weatherDataNameId').innerText;
    this.weatherapiService
      .getSubscribeWeatherAlert(location)
      .subscribe(data =>{ this.subscribeAlert = data;
      console.log(this.subscribeAlert);
      });
  }

  keyword = 'name';

  data: {
    name: string;
  }[];

  selectEvent(item) {
    // do something with selected item
  }

  onChangeSearch(event) {

    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    this.isLoadingResult = true;
    console.log("in onChangeSearch"+event);
    this.weatherapiService
      .getAutoCompleteCity(event)
      .subscribe(data =>{
      this.data = JSON.parse(JSON.stringify(data));
      //Object.assign( this.data, JSON.parse(JSON.stringify(data)) );
      this.isLoadingResult = false;
       });
  }

  onFocused(e){
    // do something when input is focused
  }

  searchCleared() {
    console.log('searchCleared');
    this.data = [];
  }

}
